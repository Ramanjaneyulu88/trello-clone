import React, { Component } from "react";
import CheckBox from "./CheckBox";
class Card extends Component {
  state = {};
  handleDelete = () => {
    //  console.log(this.props.cardDetails.id)
    this.props.deleteCard(this.props.cardDetails.id);
  };

  handleOnClick = () => {
    console.log("Clicked on card");
  };
  render() {
    const { name, id } = this.props.cardDetails;
    // console.log(id)
    return (
      <div className="eachCard">
        <div onClick={() => this.handleOnClick()}>
          <h1> {name}</h1>
          <button onClick={() => this.handleDelete()}>delete card</button>
        </div>

        <CheckBox cardName = {name} cardId={id} handleOnClick={this.handleOnClick} />
      </div>
    );
  }
}

export default Card;
