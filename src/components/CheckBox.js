import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import CheckList from "./CheckList";
import * as APICall from "../api";

class CheckBox extends Component {
  state = { checkLists: [], show: false };

  componentDidMount() {
    APICall.getCheckList(this.props.cardId).then((res) => {
      this.setState({ checkLists: res.data });
    });
  }

  deleteCheckList = (checkListId) => {
    //console.log(checkListId);
    APICall.deleteCheckList(checkListId)
      .then((res) => {
       // console.log(res.data.id);
        const newCheckList = this.state.checkLists.filter((each) => {
          return each.id !== checkListId;
        });

        this.setState({ checkLists: newCheckList });
      })
      .catch((error) => console.log(error));
  };

  createCheckList = (event) => {
    event.preventDefault();
    //console.log(event.target[0].value);
    APICall.createCheckList(event.target[0].value, this.props.cardId).then(
      (res) => {
        // console.log(res)
        this.setState({ checkLists: [...this.state.checkLists, res.data] });
      }
    );
  };
  render() {
    return (
      <>
        <Button
          onClick={() => this.setState({ show: true })}
          className="me-2"
          style={{ height: "1.9rem", padding: "0.3rem", fontSize: ".8rem" }}
        >
          CheckBox
        </Button>

        <Modal
          size="xl"
          show={this.state.show}
          onHide={() => this.setState({ show: false })}
          aria-labelledby="example-modal-sizes-title-sm"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-modal-sizes-title-sm">
              CheckList
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <Form onSubmit={(e) => this.createCheckList(e)}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Add CheckList</Form.Label>
                  <Form.Control type="text" placeholder="Enter Name" />
                </Form.Group>

                <Button variant="primary" type="submit">
                  Add
                </Button>
              </Form>
            </div>
            <div>
              {this.state.checkLists.map((each) => {
                return (
                  <CheckList
                    key={each.id}
                    eachCheckList={each}
                    deleteCheckList={this.deleteCheckList}
                    cardName = {this.props.cardName}
                  />
                );
              })}
            </div>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

export default CheckBox;
