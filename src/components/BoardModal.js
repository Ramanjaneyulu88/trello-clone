import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";

function BoardModal(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    props.createBoard(e.target[0].value);
  };

  return (
    <>
      <button
        variant="primary"
        onClick={handleShow}
        className="create-board-btn"
      >
        Create new Board
      </button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton></Modal.Header>

        <Form onSubmit={(e) => handleSubmit(e)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Enter Board Name</Form.Label>
            <Form.Control type="text" placeholder="Enter Name" />
            <Button type="submit" variant="primary">
              Add
            </Button>
          </Form.Group>
        </Form>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default BoardModal;
