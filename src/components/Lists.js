import React, { Component } from "react";
import * as APICall from "../api";
import ListItem from "./ListItem";
import AddListForm from "./AddListForm";
import ErrorMessage from "./ErrorMessage";
import Loader from "./Loader";

class Lists extends Component {
  state = { lists: [], errorMsg: "" ,isLoading:true};

  componentDidMount() {
    APICall.getLists(this.props.match.params.boardId)
      .then((data) => {
        this.setState({ lists: data,isLoading:false });
      })
      .catch((error) => {
        this.setState({ errorMsg: error.message });
      });
  }

  createList = (e) => {
    e.preventDefault();

    APICall.createList(e.target[1].value, this.props.match.params.boardId)
      .then((res) => {
        this.setState({
          lists: [...this.state.lists, res.data],
          fetchingError: "",
        });
      })
      .catch((error) => {
        console.log(error.message)
      });
  };

  deleteList = (listId) => {
    APICall.deleteList(listId).then((res) => {
      const finalList = this.state.lists.filter((each) => listId !== each.id);
      this.setState({ lists: finalList, errorMsg: "" });
    });
  };

  render() {
    if(this.state.isLoading) return <Loader/>
    if (!this.state.error) {
      return (
        <div className="lists">
          <h1 className="lists-header"> Lists</h1>
          <div className="lists-container">
            {this.state.lists.map((each) => {
              return (
                <ListItem
                  name={each.name}
                  key={each.id}
                  listId={each.id}
                  deleteList={this.deleteList}
                />
              );
            })}
            <AddListForm createList={this.createList} />
          </div>
        </div>
      );
    } else {
      return <ErrorMessage />;
    }
  }
}

export default Lists;
