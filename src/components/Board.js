import React from "react";
import { Link } from "react-router-dom";

function Board(props) {
  const { name, id } = props.eachBoard;

  return (
    <div className="each-board">
      <Link to={`/board/${id}`} style={{ textDecoration: "none" }}>
        <h1> {name}</h1>
      </Link>
    </div>
  );
}

export default Board;
