import React, { Component } from "react";

import Board from "./Board";
import BoardModal from "./BoardModal";
import ErrorMessage from "./ErrorMessage";
import Loader from "./Loader";
import * as APICall from "../api";

class Boards extends Component {
  state = { boards: [], isLoading: false, errorMsg: "" };
  componentDidMount() {
    APICall.getBoards()
      .then((data) =>
        this.setState({ boards: data, isLoading: true, errorMsg: "" })
      )
      .catch((error) => {
        this.setState({ errorMsg: error.message });
      });
  }

  createBoard = async (name) => {
    // console.log(name);
    await APICall.createBoard(name);
    APICall.getBoards().then((data) => this.setState({ boards: data }));
  };
  render() {
    if (!this.state.errorMsg) {
      if (!this.state.isLoading)
        return (
         <Loader/>
        );

      return (
        <div className="boards-container">
          {this.state.boards.map((each) => (
            <Board key={each.id} eachBoard={each} />
          ))}

          <BoardModal createBoard={this.createBoard} />
        </div>
      );
    } else {
      return <ErrorMessage />;
    }
  }
}

export default Boards;
