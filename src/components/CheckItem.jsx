import React, { Component } from 'react';


class CheckItem extends Component {
    state = {  } 
    handleOnClick = () => {
        console.log(this.props.eachCheckItem.state)
        this.props.deleteCheckItem(this.props.eachCheckItem.id)
    }
    render() { 
        let isChecked = this.props.eachCheckItem.state === 'incomplete' ?false:true
        return (
            <div className='each-checkitem'>
               {/*  <input type= 'checkbox'/> */}
               <input type= 'checkbox' readOnly checked  = {isChecked}/>
                <p> {this.props.eachCheckItem.name}</p>
                <button onClick={this.handleOnClick}>Delete checkItem</button>
            </div>
        );
    }
}
 
export default CheckItem;