import React, { Component } from "react";
import * as APICall from "../api";
import CheckItem from "./CheckItem";
class CheckList extends Component {
  state = { checkItems: [], addItemOpen: false };

  componentDidMount() {
    APICall.getCheckItems(this.props.eachCheckList.id).then((res) => {
     // console.log(res.data)
      this.setState({ checkItems: res.data });
    });
  }

  handleDelete = () => {
    // console.log(this.props.eachCheckList.id);
    this.props.deleteCheckList(this.props.eachCheckList.id);
  };

  openAddCheck = (e) => {
    this.setState({ addItemOpen: true });
  };

  addCheckItem = (event) => {
    event.preventDefault();

    APICall.createCheckItem(
      event.target[0].value,
      this.props.eachCheckList.id
    ).then((res) => {
      this.setState({ checkItems: [...this.state.checkItems, res.data] });
    });
  };

  deleteCheckItem = (checkItemId) => {
    //console.log(this.props.eachCheckList.id)

    APICall.deleteCheckItem(checkItemId, this.props.eachCheckList.id).then(
      (res) => {
        const newCheckItems = this.state.checkItems.filter(
          (each) => each.id !== checkItemId
        );
        this.setState({ checkItems: newCheckItems });
      }
    );
  };

  render() {
    return (
      <div className="checklist" key={this.props.eachCheckList.id}>
        <div>
          <p className="checklist-name">{this.props.eachCheckList.name}</p>
          <button onClick={(e) => this.handleDelete(e)}>
            Delete CheckList
          </button>
          {this.state.checkItems.map((each) => {
            return (
              <CheckItem
                key={each.id}
                eachCheckItem={each}
                deleteCheckItem={this.deleteCheckItem}
              />
            );
          })}
          <button onClick={(e) => this.openAddCheck(e)}>Add an item</button>
          {this.state.addItemOpen && (
            <div className="add-check-popup">
              <form onSubmit={(e) => this.addCheckItem(e)}>
                <input />
                <button>Add checkItem</button>
              </form>
              <button onClick={() => this.setState({ addItemOpen: false })}>
                X
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default CheckList;
