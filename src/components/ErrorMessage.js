import React from "react";

function ErrorMessage() {
  return (
    <div>
      <h1>400</h1>
      <p> Oops.. Something went wrong</p>
    </div>
  );
}

export default ErrorMessage;
