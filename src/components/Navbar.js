import * as React from "react";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";

const TrelloNavbar = (props) => {
  return (
    <>
      <Navbar bg="primary" variant="dark">
        <Container>
          <Navbar.Brand href="#home">Trello</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="#home">Workspace</Nav.Link>
            <Nav.Link href="#features">Recent</Nav.Link>
            <Nav.Link href="#pricing">Starred</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default TrelloNavbar;
