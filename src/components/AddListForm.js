import React, { useState } from "react";

function AddListForm({ createList }) {
  const [isOpen, setIsOpen] = useState(false);

  const addList = (e) => {
    createList(e);
    setIsOpen(false);
  };

  return (
    <div>
      <button className="addAnotherListBtn" onClick={() => setIsOpen(true)}>
        {" "}
        + Add another list{" "}
      </button>

      {isOpen && (
        <form onSubmit={(e) => addList(e)} className="addlist-form">
          <button type="submit">Add List</button>

          <input type="text" placeholder="Enter list title" />
        </form>
      )}
    </div>
  );
}

export default AddListForm;
