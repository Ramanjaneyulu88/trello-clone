import React, { Component } from "react";
import * as APICall from "../api";
import Card from "./Card";

class ListItem extends Component {
  state = { cards: [], errorMsg: "" };
  componentDidMount() {
    APICall.getCardsInAList(this.props.listId)
      .then((data) => {
        // console.log(data);
        this.setState({ cards: data, errorMsg: "" });
      })
      .catch((error) => {
        this.setState({ errorMsg: error.message });
      });
  }

  createCard = (e) => {
    e.preventDefault();
    //console.log(e.target[0].value);
    APICall.createCard(e.target[0].value, this.props.listId).then((res) => {
      //console.log(res)
      this.setState({ cards: [...this.state.cards, res.data] });
    });
  };

  deleteHandle = () => {
    this.props.deleteList(this.props.listId);
  };

  deleteCard = (cardId) => {
    APICall.deleteCard(cardId).then((res) => {
      const newCards = this.state.cards.filter((each) => each.id !== cardId);
      this.setState({ cards: newCards });
    });
  };

  render() {
    return (
      <div className="list-container">
        <h1 className="list-header">{this.props.name}</h1>
        <button
          className="delete-list-button"
          onClick={(e) => this.deleteHandle()}
        >
          Delete List
        </button>
        {this.state.cards.map((each) => {
          return (
            <Card
              key={each.id}
              cardDetails={each}
              deleteCard={this.deleteCard}
            />
          );
        })}
        <form onSubmit={(e) => this.createCard(e)} className="addcard-form">
          <input type="text" placeholder="card name" />
          <button className="addcard-button">Add Card</button>
        </form>
      </div>
    );
  }
}

export default ListItem;
