const axios = require("axios");
let token = "6daef8d3323d112b6f001830de7038977dbaa088a323b01499b5a97b3f5ceeff";
let key = "2f90ce648ca7fde6fb00a3e4d79d16b0";

export function getBoards() {
  return axios
    .get(
      `https://api.trello.com/1/members/me/boards?fields=name,url&key=${key}&token=${token}`
    )
    .then((res) => res.data);
}

export function createBoard(name) {
  return axios
    .post(
      `https://api.trello.com/1/boards/?name=${name}&key=${key}&token=${token}`
    )
    .then((res) => {
      // console.log(res)
      return res;
    });
}

export function getLists(listId) {
  return axios
    .get(
      `https://api.trello.com/1/boards/${listId}/lists?key=${key}&token=${token}`
    )
    .then((res) => {
      return res.data;
    });
}

export function createList(name, boardId) {
  return axios
    .post(
      `https://api.trello.com/1/boards/${boardId}/lists?name=${name}&key=${key}&token=${token}`
    )
    .then((res) => {
      //  console.log(res);
      return res;
    });
}

export function getCardsInAList(listId) {
  return axios
    .get(
      `https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`
    )
    .then((res) => {
      //  console.log(res);
      return res.data;
    });
}

export function createCard(name, idList) {
  return axios
    .post(
      `https://api.trello.com/1/cards?idList=${idList}&name=${name}&key=${key}&token=${token}`
    )
    .then((res) => {
      // console.log(res)
      return res;
    });
}

export function deleteList(listId) {
  return axios.put(
    `https://api.trello.com/1/lists/${listId}/closed?key=${key}&token=${token}&value=true`
  );
}

export function deleteCard(cardId) {
  return axios
    .delete(
      `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`
    )
    .then((res) => res);
}

export function createCheckList(name, cardId) {
  return axios.post(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}&name=${name}`
  );
}

export function getCheckList(cardId) {
  return axios.get(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`
  );
}

export function deleteCheckList(checkListId) {
  return axios.delete(
    `https://api.trello.com/1/checklists/${checkListId}?key=${key}&token=${token}`
  );
}

export function getCheckItems(checkListId) {
  return axios.get(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${key}&token=${token}`
  );
}

export function createCheckItem(name, checkListId) {
  return axios.post(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${name}&key=${key}&token=${token}`
  );
}


export function deleteCheckItem(checkItemId,checkListId){
  return axios.delete(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${key}&token=${token}`
  )
}