import React, { Component } from "react";
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom/cjs/react-router-dom.min";
import Lists from "./components/Lists";
import TrelloNavbar from "./components/Navbar";
import Boards from "./components/Boards";

import "./App.css";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <TrelloNavbar />
        <Switch>
          <Route exact path="/" component={Boards} />
          <Route path="/board/:boardId" component={Lists} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
